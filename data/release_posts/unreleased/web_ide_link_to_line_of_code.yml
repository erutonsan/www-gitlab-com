---
features:
  secondary:
  - name: "Deep link directly to lines of code"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/project/repository/web_editor.html#highlight-lines'
    image_url: '/images/unreleased/web-ide-link-to-line.png'
    reporter: ericschurter
    stage: create
    categories:
    - 'Web IDE'
    - 'Pipeline Authoring'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/21762'
    description: |
      Collaborating on long files is easier when you can share a link to a specific line of code. Previously, you could only link to a line number when viewing a file in the repository, but now you can also link to specific lines of code in the context of editing a file.

      To get a link to a specific line, click the icon in the gutter next to the line number, or append `#L` and the line number to the edit link. The link will open the editor, scroll to the line, and highlight it for you. Try it out in the [single-file editor](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/source/direction/create/web_ide/index.html.md#L81), [the Web IDE](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/direction/create/web_ide/index.html.md#L81), and the [Pipeline Editor](https://gitlab.com/gitlab-com/www-gitlab-com/-/ci/editor#L151)!

      Tip: you can even create a link to multiple lines by appending a range to the link, like `#L87-98`, even though the UI doesn't support creating these (yet).
