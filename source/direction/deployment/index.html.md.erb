---
layout: markdown_page
title: "Deployment Direction"
description: "The job of deploying software is a critical step in DevOps. This page highlights the GitLab's direction."
canonical_path: "/direction/deployment/"
---

- TOC
{:toc}

<%= devops_diagram(["Configure","Release"]) %>

## Overview

### What is Deployment?

Deployment is the step to which brings coded, integrated, and built software to a point where it can receive feedback from the most important stakeholders - users. Deployment is part of GitLab's [Ops](/direction/ops/) section.

Deployments are increasing in form and freqency. 
* Deployment **frequency** is increasing because of competitive pressures for organizations to tighten user feedback loops. Adoption of Agile and Devops practices and culture enable firms to deploy every minute. 
* Deployment **form** is changing because of the adoption of cloud infrastructure, container orchestration engines and GitOps. Infrastructure as Code and GitOps enables new infrastructure for applications to be deployed in minutes and new application changes to be seamlessly and progressively pullled into existing infrastructure.

One side effect of this evolution is that the line between the configuration of infrastructure and the release of software has been blurred. In order to clearly communicate our ambition and vision for these stages, GitLab's deployment direction is inclusive of both the [Configure](/direction/configure/) stage and the [Release](/direction/release/) stage to account for the advancement in deployment practices. 

### Deployment Vision

GitLab's deployment vision is to empower deployment to be fast and easy, yet flexible enough to support the scale and operating model for your business.

We want you to spend the majority of your time creating new software experiences for your users instead of investing time and effort on figuring out how to get it into their hands. No matter if you are operating your own infrastructure or are cloud-native, GitLab is your one stop-deployment-shop from AutoDevOps to building your organizations own platform-as-a-service.

### Market
The total addressable market (TAMkt) for DevOps tools targeting the Release and Configure stages was [$1.79B in 2020 and is expected to grow to $3.25B by 2024 (13.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035). This analysis is considered conservative as it focuses only on developers and doesn't include additional potential users. External market sizing show a much bigger potential. For example, for continous delivery alone, which does not include categories such as infrastructure as code, has a [market size of $1.62B in 2018 growing to $6B by 2026 (17.76% CAGR)](https://www.verifiedmarketresearch.com/product/continuous-delivery-market/). This, like the [Ops market](/direction/ops/#market) in general, is a deep value pool and represents a significant portion of GitLab's expanding addressable market. 

The deployment tooling market has also evolved. There are now more options than adding a delivery or deployment job to your CI/CD pipeline. With tools like Kubernetes becoming essential for most modern cloud deployments and companies adopting their take on DevOps culture, there is high demand for cloud-native CD.    



Ignore below this point

### Current Position

## Challenges

## Opportunities

* Multi-cloud
* Hybrid cloud
* No-Ops 
* Everything as code
* Cross-Use case workflows (currently in Ops Direction)
* Choice proliferation

## Important areas

* Support for GitOPs
* Observability built-in
* Release management/advanced deployment/feature flags

## Competitive Landscape

### GitHub Actions

[GitHub Actions](https://github.com/features/actions) are an automation workflow integrated with GitHub repositories. Each action is triggered by one or multiple events.
Using GitHub Actions, users can define any number of jobs, including deployment jobs. Microsoft has etup actions to be like lego pieces, and has built a workflow around finding and reusing actions in a [actions marketplace](https://github.com/marketplace?type=actions). 

GitHub Actions recently introduced [environments](https://github.blog/changelog/2020-12-15-github-actions-environments-environment-protection-rules-and-environment-secrets-beta/) which can help users set specific rules based on environments to automate deployment workflows. Furthermore, environment scoped secrets enables different secrets for different tiers, separating deployment from development to meet compliance and security requirements.

### Harness

[Harness](https://harness.io/) (with their recent acquisition of [Drone.io](https://harness.io/continuous-integration/)) is a modern, ambitious CI/CD platform that can be a single platform to build, test, deploy and verify any applications. Harness integrates with tools like Service Now and Jira to enable approval workflows. It integrates with monitoring tools (like DataDog, Splunk or CloudWatch) and applies AI to metrics to automate deployment workflows like rollbacks. Lastly, it is intuitively easy to create dashboards like the DORA4 to provide feedback.

Harness provides a more visual and template based CD pipeline definition process than GitLab, enabling developers get up and running with only a few clicks to complete a end-to-end deployment platform.

### Spinnaker

[Spinnaker](https://spinnaker.io/), born out of Netflix, is an open-source, cloud-native, multi-cloud continuous delivery platform for releasing software changes.

It views its solution in three parts. First, [application management](https://spinnaker.io/concepts/#application-management-aka-infrastructure-management) which enables users to visualize and manage cloud resources. Second, [application deployment](https://spinnaker.io/concepts/#application-deployment) is a powerful and flexible pipeline management system with integrations to the major cloud providers and treats deployments as a first-class citizen. Lastly, with [managed delivery](https://spinnaker.io/concepts/#managed-delivery) combines application management and delivery, and enables users to specify what they want in declaritive format.

### Waypoint

[Waypoint](https://www.waypointproject.io/), by Hashicorp, provides a modern workflow to build, deploy, and release across platforms. Waypoint uses a single configuration file and common workflow to manage and observe deployments across platforms such as Kubernetes, Nomad, EC2, Google Cloud Run, and more. It maps artifacts to runtime after the test and build phases. Waypoint sits alongside the source code, and enables declarative deployment - it takes the manifest of how the platform is configured and will execute the steps sequentially unique for each platform. Waypoint can be used with GitLab CI and even with the Auto DevOps workflow with deployments being done by Waypoint. 

### Argo CD

[Argo CD](https://argoproj.github.io/argo-cd/) is a declarative, GitOps continuous delivery tool for Kubernetes. It is part of the [Argo Project](https://argoproj.github.io/) which is a incubation project in CNCF. Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes. Argo CD is installed as an application in its own namespace in a cluster. Optionally, you can also register addition clusters to do multi-cluster deployments.

Beyond enabling users to practice GitOps, Argo CD does a great job reporting and visualizing the differences between your target state and live state, which greatly facilitates learning and understanding for operators.

### Weaveworks

[Weaveworks](https://www.weave.works/) positions itself as a Kubernetes platform, enabling devops teams to do GitOps, to manage cluster lifecycles, and to monitor applications. Weaveworks operates a SaaS solution that operates the various open source projects it maintains. Of the open source tools, particularly notable in the deployment context is [Flux](https://www.weave.works/oss/flux/) which enables GitOps, [Flagger](https://www.weave.works/oss/flagger/) which enables canary deployments. [Scope](https://www.weave.works/oss/scope/) which enables visualization of Kubernetes, and [Cortex](https://www.weave.works/oss/cortex/) which enables monitoring.

### Jenkins X
