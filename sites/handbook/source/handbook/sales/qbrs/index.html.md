---
layout: handbook-page-toc
title: "GitLab Sales QBRs"
description: "Overview of GitLab's Quarterly Business Review process"
---

## Quarterly Business Review (QBR) Overview
QBRs are held during the **first month of every quarter.** The goal of these sessions is to: 
1. Review performance and lessons learned from the past quarter
1. Review current quarter forecast and plans to meet and exceed sales goals in the coming quarter (and beyond)
1. Identify and prioritize specific requests that will help Sales be successful

### QBR Schedules
QBRs are held in the following months and typically are a mix of in-person and virtual formats. This is subject to change based on any outside conditions that make it unsafe for the team to travel (i.e. COVID-19).
1. Feb: In person (co-located with Sales Kick Off)
1. May: Virtual
1. Aug: In-person (virtual in FY21 due to COVID)
1. Nov: Virtual

QBRs are typically organized by region, and the agenda is determined by the Regional Directors (RDs) and/or Area Sales Managers (ASMs) for those regions. In addition, Quarterly Sales Leadership QBRs will be held following the regional QBRs.

**ENT Sales QBRs**
1. APAC
1. EMEA: 
    - UKI, Netherlands, and Scandinavia
    - DACH, and South
1. AMER East
    - Central
    - Northeast
    - Southeast
    - Named
1. AMER West
    - NorCal
    - Pacific NW & Midwest (may split in the future)
    - Rockies & SoCal
1. US PubSec
    - SLED
    - National Security Group (NSG includes DHS, DoJ, and DoE)
    - DoD
    - Civilian
1. ENT Sales Leadership

**COMM Sales QBRs**
1. Mid Market Americas East
1. Mid Market Americas West
1. Mid Market APAC & EMEA
1. SMB Americas East
1. SMB Americas West
1. SMB APAC & EMEA
1. COMM Sales Leadership

**Channel & Alliances QBRs**
1. WW Channels 
1. Alliances

**CS QBRs**
1. Customer Success Leadership (VP directs)
1. Segment Sales VP
1. Chief Revenue Officer (CRO)
1. CRO direct reports
1. VP of Product Management
1. VP of Field Ops direct reports
1. People Business Partners
1. Sr. Program Manager, Customer Success Enablement
1. VP of Finance 
1. Director of Finance (supporting CS)
1. VP of IT (Optional)
1. Director of Data (Optional)

**ENT Leadership QBR**
1. Segment Sales VP
1. Segment Sales VP direct reports (RDs, ASMs, and Inside Sales Managers)
1. Chief Revenue Officer (CRO) and CRO EBA (both optional)
1. Chief Marketing Officer (CMO) (optional)
1. CRO direct reports
1. VP of Demand Gen
1. VP of Revenue Marketing
1. VP of Product Management
1. VP of Field Ops direct reports
1. Sr. Program Manager, ENT Sales Enablement
1. Sr. Program Manager, Customer Success Enablement (optional)

**COMM Leadership QBR**
1. Segment Sales VP
1. Segment Sales VP direct reports (RDs, ASMs)
1. Chief Revenue Officer (CRO) and CRO EBA (both optional)
1. Chief Marketing Officer (CMO) (optional)
1. CRO direct reports
1. VP of Demand Gen
1. VP of Revenue Marketing
1. VP of Product Management
1. VP of Field Ops direct reports
1. Field Marketing leadership
1. All SDR Managers
1. People Business Partners
1. Manager, Product Marketing
1. Sr. Program Manager, COMM Sales Enablement
1. Sr. Program Manager, Customer Success Enablement (optional)

**CS Leadership QBR**
1. VP Customer Success
1. VP Customer Success direct reports
1. Chief Revenue Officer (CRO) and CRO EBA (both optional)
1. Chief Marketing Officer (CMO) (optional)
1. CRO direct reports
1. VP of Demand Gen
1. VP of Revenue Marketing
1. VP of Product Management
1. VP of Field Ops direct reports
1. People Business Partners
1. Manager, Product Marketing
1. Sr. Program Manager, Customer Success Enablement

### QBR Participation
QBRs include account teams, leadership, and delegates from across the business: 
1. All Strategic Account Leaders (SALs) and Account Executives (AEs), Solution Architects (SAs), Technical Account Managers (TAMs), and Channel Account Managers
1. All Sales, Channel, and Customer Success Managers
1. 1-2 delegates from the following teams will participate
   1. Field Marketing
   1. Demand Generation (Sales Dev)
   1. Product Marketing
   1. Product / Engineering
   1. Alliances
   1. Sales Ops
   1. Sales Strategy
   1. Field Enablement
   1. Finance
   1. People Group
   1. Field Recruiting Team
   1. Customer Reference 
1. Field executives (VP+) will participate in these sessions when possible

## QBR Best Practices

### Best Practices for RDs/ASMs/SALs/AEs
1. One ASM per day for remote QBRs.
1. ASMs serve as the DRIs for attendance in their sessions. 
1. QBRs should be an inclusive environment – sales team members should encourage participation from other members of their account teams as well as delegates from other areas of the business. 
   1. Invite your SDR Manager, Field Marketing delegate, and TAM Manager to provide a brief (5 minute) update during each QBR.
   1. Channel & Alliances delegates should be given 15-20 minutes in the agenda for each QBR to present on progress in the region. 
1. Any changes to the QBR schedules should be made no later than two weeks before the scheduled start of QBRs to avoid calendar disruptions for participants. 
1. All SALs/AEs should complete their QBR decks no later than 2 business days prior to their QBR session. (Note: For QBRs that occur on the first business day of the new quarter, the standard is one business day.)
1. All RDs/ASMs/SALs/AEs should pre-read other QBR decks for their session. Questions should be left ahead of time in the "Speaker Notes" section of the related slide. 
   1. ASMs should review their team's QBR asks and pre-prepare a list of prioritized asks to review in their QBR session. (See the [Field Operations QBR Request Intake Process](/handbook/sales/qbrs/#intake) below for more information.)

### Best Practices for Attendees 
1. Please join on time and ensure your microphone is muted to avoid disruptions. 
1. Please be an active participant in the QBR session – join on video (if possible), answer questions related to your area of expertise/team, and participate in discussions. 
1. Please contribute by taking notes during the QBRs. Everyone can (and should!) contribute, but delegates from these teams are expected to pitch in on note taking: 
   1. Sales Operations
   1. Field Enablement 
   1. Product Marketing
   1. Field Marketing
   1. Sales Dev
   1. Channel
   1. Alliances
1. All sessions will be recorded and there will be a verbatim written transcription available via Chorus, so notes taken during QBRs should focus on key takeaways and value-add commentary and conversation. See [Note Taking](/handbook/sales/qbrs/#qbr-note-taking) section below for full note taking guidance.  

## QBR Communication
1. QBR-related Slack channels will be created no later than five weeks before the start of QBRs.
   1. The naming will follow this format: 
      1. #qbr-comm-qxfyxx
      1. #qbr-ent-qxfyxx
      1. #qbr-channel-qxfyxx
   1. These channels will be used to:
      1. Coordinate asks/reminders for RD/ASM planning action items. 
      1. Send reminders about QBR schedules and timing.
      1. Field questions about attendance.
      1. Field logistical questions (i.e. "what's the Zoom link?") that arise as QBRs are in process.
      1. Conduct follow-ups to QBR asks. (i.e. share how/where we have tracked QBR asks and inform the field of the process that will happen next to address them and where to look for updates)
   1. Uses do *not* include:
      1. Facilitate manager approval or feedback for individual reps' QBR decks. This is meant to be handled by managers with their teammates via their own preferred channels.
      1. Make any QBR asks that should be included in reps' decks.
   1. These channels will be archived within 60 days of QBRs being completed.

### QBR Note Taking
The purpose of notes is to capture meaningful insights that come out of the *conversation* during a QBR - questions and answers, feedback, asks, etc. Do not capture verbatim notes or data that is already documented on a slide. The video recording of the meeting will show the slides.

**Q: Who should take notes?**

A: If you are attending as a delegate, please note that it is part of your responsibility to contribute to note-taking in the QBR. 

**Q: What specific information should the delegate capture?**

A: In order to serve as an active participant in QBRs, there is no need to take verbatim notes during the sessions. 
1. Make as detailed notes as possible *pertaining to asks, action items, or specific improvement requests*. Please include reference to the specific slide in the rep's presentation that pertains to the ask when possible.
1. Capture meaningful insights that come from conversations during the QBR, not information already written in a slide.

**Q: How will the rest of the information be captured?**

A: All QBR hosts are required to record their sessions and provide a link to the recording for the rest of the team and company to access. This reduces the need to take verbatim notes.

### Where to Find Past QBRs 
1. A list of all QBR recordings will be housed in a tab on each segment's (ENT, COMM, Channel) schedule spreadsheet. 
1. A full list of past QBR presentations can be found in the [QBR Google drive folder](https://drive.google.com/drive/u/0/folders/1QAWFYxT2-NzWpOUodepbl0O6zfsMxJGL).

## Field Operations QBR Request Process

### Intake

1. Reps are encouraged to share their top asks for leadership and supporting teams as a part of their QBRs. The asks will be prioritized by segment (ENT & COMM) leadership before being passed off to Sales Ops. 
   1. Enterprise Ask Prioritization Process
      1. SAL includes a slide of asks in their QBR presentation
      1. ASMs review all asks for their region holistically upon pre-review of QBR decks and identify a pattern of asks. 
         1. ASM-identified top asks will be discussed during their QBR session. Final list of prioritized asks will be passed on to their RD.
      1. RDs will review and prioritize their region's top asks and present them in the ENT Leadership QBR. 
      1. RDs and ENT Sales VP will review full list of ENT asks in subsequent RD meeting and will deliver a final list of prioritized ENT asks (no more than 5 asks per functional group) no later than the Monday following the ENT Leadership QBR. 
   1. Commercial Ask Prioritization Process 
      1. AE includes a slide of asks in their QBR presentation
      1. ASMs review all asks for their region holistically upon pre-review of QBR decks and identify a pattern of asks. 
         1. ASM-identified top asks will be discussed during their QBR session to identify a final list of prioritized asks.
      1. ASMs will present their region's top asks in the COMM Leadership QBR. 
      1. ASMs and COMM Sales VP will review full list of COMM asks and will deliver a final list of prioritized COMM asks no later than the Monday following the COMM Leadership QBR. 
   1. Note that SALs/AEs are not expected to read/dig in to each ask as this will primarily be duplicative to the consolidated regaional asks list shared by the ASMs during the sessions. The ASMs are encouraged to discuss any asks that were not included in the final list with the requestor in their 1:1 to determine how/if any action should be taken. 
      1. Reps can/should make asks of Field Ops at any time outside of QBRs using [this process] (link?).
1. Sales Ops provides representative coverage at each QBR, and captures a list of prioritized Sales Asks/Requests (google sheet/notes) with Links to individual QBR presentations.
1. Following each QBR session, prioritized Sales asks are converted to Issues using the [Field-Ops-QBR-Request](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=FieldOpsQBR) Template.
	- Issues opened through above template, will automatically apply the (global) `QBR` label to ensure they’re categorized appropriately for review.

### Execution and Tracking

All new QBR request to the Sales Operations team are tracked and managed as issues within the Sales Operations Project. The [Field Ops QBR Requests board](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1433489?&label_name[]=QBR) tracks the status of these Issues.

If you need more immediate attention please send a message with a link to the issue you created in the `#sales-support` Slack channel.

![Field Ops QBR Request Flow](/handbook/sales/images/SalesOps_QBR_Request_Flow.jpg)

1. **New requests** start with the project label  `SalesOps::New_Request`, which should be auto-assigned if the template was used. Note these should still be **unassigned**.
1. Issues are then moved to `SalesOps::Triage` where they are **triaged** to the correct Sales Ops team (Sales Ops, CS, Deal Desk, Sales Systems), who will determine if there is adequate detail to plan and prioritize the work.
	  - Is the problem statement clear?
	  - Is the impact/value understood?
1. Issues will then be moved to one of the following:
	- `SalesOps::Backlog` - For future scheduling, sequencing, or implementation.
	- `SalesOps::Transferred` - Issues related to teams other than Sales Ops.
	- `SalesOps::Assigned` - for **urgent** requests or quick-wins.
	- `SalesOps::Declined` - in some cases Sales Ops may directly decline the request.
1. From the backlog issues will be moved to:
	- `SalesOps::Assigned` - Issue is assigned to a team member and being executed.
	- `SalesOps::Declined` - If the backlog issue can no longer be accommodated, no longer makes sense to do, etc. it should be moved to declined, and then **closed**.
1. Once **completed**, issues should be moved to `SalesOps::Completed`, and then **closed**.

## Sample QBR Roll-Out 
Below is a sample roll-out of the QBR planning and execution process that is followed each quarter. This process is subject to change based on sales leadership feedback and priorities. If you have questions, please reach out to #field-enablement-team on Slack.

#### 1 month before end of Q4 (each FY)
1. Field Enablement to present a tentative schedule of QBRs for the following 4 quarters.
   1. Socialize to ENT and COMM VPs for buy-in
   1. Once approved, socialize to all RDs and ASMs for buy in

#### 5 weeks before QBRs are scheduled to start
1. Field Enablement to finalize initial QBR Schedule, Attendee and Agenda document. (See past documents in the [Field Operatons Google Drive folder](https://drive.google.com/drive/u/0/folders/1Zy1AFhJoHo3_nXCiKU8gtM3a1UBBJu6Y).)
   1. Field enablement with provide a sample agenda for ENT and COMM segments to be used as a reference point for ASMs. 
1. Field Enablement to create a QBR issue that will be used to share:
   1. Agenda documents 
   1. Deck templates
   1. Notes documents for each session
   1. A checklist of key dates and milestones
1. Field Operations will host a "QBR Kickoff Call" with the CRO, ENT and COMM VPs, and all RDs/ASMs to overview the upcoming QBR planning process and reiterate asks/responsibilities. (All field managers will be added as optional.)
1. Field Enablement to stand up QBR-specific Slack channels to address questions from field managers and coordinate RD/ASM asks. 
1. RDs/ASMs to provide input in the following areas by EOW.
   1. Final approval/check on the assigned date for their QBR
   1. A full list of attendees – team members, relevant SAs/TAMs, any delegates they want to ensure are included (SDRs, PMMs, etc.)
      1. Please note that RDs/ASMs are not required to select delegates from other teams for their sessions. This is a chance to request a specific team member (i.e. a specific team member from Strategic Marketing). If an RD/ASM chooses not to, Field Enablement will work with managers from these teams to select delegates for each session. 
   1. Logistical details (i.e. personal Zoom room link)

#### 4 weeks before QBRs are scheduled to start
1. Field Enablement to send out QBR calendar hold invites that include the following attendees:
   1. RDs, ASMs, team members for that region - mandatory
   1. 1 Delegate from the following teams - mandatory
      1. Sales Dev
      1. Field Marketing
      1. Product Marketing
      1. Sales Ops
      1. Field Enablement
      1. Channel/Alliance
   1. GTM leaders - Optional
      1. Field VPs + CMO & direct reports
      1. VP Product Management
   1. McB - Optional
1. ENT Sales and COM Sales Enablement Program Managers (with support from Sales Ops) begin working with ENT and COMM VPs on QBR deck templates. Templates should include:
   1. Team slide – Territory coverage + account team

#### 3 weeks before QBRs are scheduled to start
1. Field Enablement to reach out to the leaders of the following areas and ask them to assign delegates for all sessions by EOW:
   1. Sales Dev
   1. Field Marketing 
   1. Product Marketing 
   1. Sales Ops
   1. Field Enablement 
   1. Channels 
   1. Alliances

#### 2 weeks before QBRs are scheduled to start
1. Further scheduling changes for QBR sessions are discouraged in order to avoid disruptions for attendees. 
1. ENT Sales and COM Sales Enablement Program Managers (with support from Sales Ops) finalize QBR deck templates with ENT and COMM VPs. QBR deck templates distributed to managers for them to cascade down to their teams.
   1. Sales Ops to provide QBR Data Packs to ENT and COMM leadership as a resource to help reps collect the necessary data for their presentations. 
1. Field Enablement to work with RDs/ASMs to create the agenda for their sessions by EOW.
1. Field Comms to send a note to the [#field-fyi Slack channel](/handbook/sales/sales-google-groups/field-fyi-channel/) informing team members that QBR templates are ready and that the QBR Slack channels are open to join for updates and any questions.

#### 1 week before QBRs are scheduled to start
1. Sales Ops to create master notes spreadsheet and notes documents for each QBR. Notes document to include the following:
   1. Note Taking Best Practices
   1. Example notes section
   1. Link to final QBR schedule 
1. Field Enablement to finalize all calendar invites with updated information – final schedule, notes document.

#### Week(s) of QBRs
1. The RD/ASM will lead the meeting and be responsible for recording the QBR.
1. Assigned note takers will take QBR notes following the [best practices outlined above.](/handbook/sales/qbrs/#qbr-note-taking)
1. Field Operations should consistently monitor the QBR Slack channels for questions.

#### 1 week following QBRs
1. RDs/ASMs should share a link to their QBR recording in the designated "QBR Recordings" tab of their segment's (ENT, COMM, Channel) schedule spreadsheet.
   1. Note: Where the recording lives is up to the RD/ASM – Zoom link, YouTube link, Chorus link – as long as the link can be accessed by Field team members.
1. Sales Ops team begins triaging and actioning on QBR asks. Execute on top-priority asks in a Cannonball run. 
