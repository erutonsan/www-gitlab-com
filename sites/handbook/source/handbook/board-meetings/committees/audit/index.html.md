---
layout: handbook-page-toc
title: "Audit Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Audit Committee Composition

- **Chairperson:** Karen Blasing
- **Members:** Bruce Armstrong, David Hornik
- **Management DRI:** Chief Financial Officer

## Audit Committee Charter

**THIS AMENDED AND RESTATED CHARTER WAS APPROVED BY THE BOARD ON 2021-03-18**

1. **Purpose**. The purpose of the Audit Committee (the "**Committee**") of the Board of Directors (the “**Board**”) of GitLab Inc. (the “**Company**”) is to assist the Board in fulfilling its oversight responsibilities relating to:
    - the Company’s accounting and financial reporting processes and internal controls, including audits and the integrity of the Company’s financial statements; 
    - the qualifications, independence and performance of the Company’s independent auditors (the “**Independent Auditors**”); 
    - risk assessment and management; and 
    - compliance by the Company with legal and regulatory requirements. This charter (the “**Charter**”) sets forth the authority and responsibility of the Committee in fulfilling its purpose. The function of the Committee primarily is one of oversight. 
1. **Structure and Membership**
    - **Members.** The Audit Committee shall consist of at least three or more members of the Board, with the exact number determined by the Board. All members of the Committee will be appointed by the Board and will serve at the Board’s discretion.
    - **Chair.** The Board shall elect a Chair of the Committee, the Committee shall elect a Chair by majority vote.
    - **Selection and Removal.** Members of the Committee may be replaced or removed by the Board at any time, with or without cause. Resignation or removal of a director from the Board, for whatever reason, will automatically constitute resignation or removal, as applicable, from the Committee.
    - **Member Qualifications.** Members of the Committee must meet the following criteria as well as any additional criteria required by applicable law, the rules and regulations of the U.S. Securities and Exchange Commission (the “Commission”) or such other qualifications as are established by the Board from time to time:
       - Each member of the Committee must meet the independence requirements of the Commission. 
       - Each member of the Committee must be financially literate, as such qualification is interpreted by the Board in its business judgment, or must become financially literate within a reasonable period of time after his or her appointment to the Committee. 
1. **Authority and Responsibilities**
    - **General.** 
       - The principal responsibilities and duties of the Committee are set forth below. These responsibilities and duties are set forth as a guide, with the understanding that the Committee will carry them out in a manner that is appropriate given the Company’s needs and circumstances. The Committee may perform such other functions as are consistent with its purpose and applicable law, rules and regulations, as the Board may request or prescribe, or as the Committee deems necessary or appropriate consistent with its purpose.
       - The Committee shall discharge its responsibilities, and shall assess the information provided by the Company’s management and the Indepedent Auditors, in accordance with its business judgment. Management is responsible for the preparation, presentation, and integrity of the Company’s financial statements and for the appropriateness of the accounting principles and reporting policies that are used by the Company. The Independent Auditors are responsible for auditing the Company’s financial statements. The authority and responsibilities set forth in this Charter do not reflect or create any duty or obligation of the Committee to plan or conduct any audit, to determine or certify that the Company’s financial statements are complete, accurate, fairly presented, or in accordance with generally accepted accounting principles or applicable law, or to guarantee the Independent Auditors’ reports.
    - **Financial Statements and Disclosures.** The Committee will:
       - Review and discuss the following with management, the internal auditors (if any), and the Independent Auditors, as applicable: 
          - a. the Company’s annual audited and quarterly unaudited financial statements; 
          - b. the results of the Independent Auditors’ audit or review of the financial statements; 
          - c. all critical audit matters (CAMs) proposed by the Independent Auditor to be included in the Independent Auditor’s annual audit report; 
          - d. any items required to be communicated by the Independent Auditors in accordance with the applicable requirements of the Public Company Accounting Oversight Board (the “PCAOB”); and
          - e. any significant issues, events and transactions as well as any significant changes regarding accounting principles, practices, policies, judgments or estimates. 
    - **Intrnal Controls** With respect to the Company’s internal controls, the Committee will: 
          - i. Review and discuss with the Company’s management, its internal auditors (if any), and the Independent Auditors, and provide oversight over, the design, implementation, adequacy and effectiveness of the Company’s accounting and financial processes and systems of internal controls and material changes in such controls, including any control deficiencies, significant deficiencies and material weaknesses in their design or operation. 
          - ii. Review any allegations of fraud that are disclosed to the Committee involving management or any Company team member with a significant role in the Company’s accounting and financial reporting process and systems of internal controls. 
          - iii. Discuss any comments or recommendations of the Independent Auditors outlined in their annual management letter or internal control reports. 
          - iv. Periodically consult with the Independent Auditors out of the presence of the Company’s management about internal controls, the fullness and accuracy of the Company’s financial statements and any other matters that the Committee or the Independent Auditors believe should be discussed privately with the Committee. 
          - v. Establish procedures for (a) the receipt, retention and treatment of complaints received by the Company regarding accounting, internal accounting controls or auditing matters, and (b) the confidential anonymous submission by employees of the Company of concerns regarding questionable accounting or auditing matters. Oversee the review of any such complaints and submissions that have been received, including the current status and the resolution if one has been reached. 
    - **Independent Auditors.** With respect to the Company’s Independent Auditors, the Committee will: 
          - i. Be directly responsible for the selection, appointment, discharge, compensation, retention and oversight of the work of the Independent Auditors and any other registered public accounting firm engaged for the purpose of preparing and issuing an audit report or performing other audit-related services for the Company. The Independent Auditors will report directly to the Committee. 
          - ii. Review and discuss with the Independent Auditors and management (a) any significant audit problems or difficulties, including difficulties encountered by the Independent Auditors during their audit work (such as restrictions on the scope of their activities or their access to information), (b) any significant disagreements between management and the Independent Auditors and (c) management’s response to these problems, difficulties or disagreements. 
          - iii. Review the qualifications, performance and continuing independence of the Independent Auditors, including: 
             - a. obtaining and reviewing, on an annual basis, a letter from the Independent Auditors describing (1) all relationships between the Independent Auditors and the Company required to be disclosed by applicable requirements of the PCAOB, (2) the Independent Auditor’s internal quality control procedures, and (3) any material issues raised by the most recent internal quality control review, peer review or PCAOB review or inspection of the firm or by any other inquiry or investigation by governmental or professional authorities; 
             - b. reviewing and discussing with the Independent Auditors relationships or services (including permissible non-audit services) that may affect their objectivity and independence; 
             - c. overseeing the rotation of the Independent Auditors’ lead audit and concurring partners and the rotation of other audit partners, with applicable time-out periods, in accordance with applicable law; and
             - d. taking such other appropriate actions as may be required or desirable by the Committee to oversee the independence of the Independent Auditors. 
          - iv. Review the Independent Auditors’ annual audit plan, scope of audit activities and staffing. 
          - v. Approve the fees and other compensation to be paid to the Independent Auditors (or other registered public accounting firms) and pre-approve all audit and non-audit related services provided by the Independent Auditors (or other registered public accounting firms) permitted by applicable law or regulation. The Committee may establish pre-approval policies and procedures, as permitted by applicable law or regulation, for the engagement of the Independent Auditors (or other registered public accounting firms) to render services to the Company including, without limitation, policies that would allow the delegation of pre-approval authority to one or more members of the Committee. 
          - vi. Review and discuss with the Independent Auditors the reports delivered to the Committee by the Independent Auditors regarding: 
             - a. critical accounting policies, estimates and practices used; 
             - b. alternative treatments of financial information within GAAP that have been discussed with management, the ramifications of those alternatives and the treatment preferred by the Independent Auditors; 
             - c. any material written communications between the Independent Auditors and the Company’s management; and 
             - d. any matters required to be communicated to the Committee under GAAP and other legal or regulatory requirements. 
    - ** Risk Oversight and Compliance:** The Committee will: 
          - i. Review with management the Company’s major financial risks and enterprise exposures and the steps management has taken to monitor or mitigate such risks and exposures, including the Company’s procedures and any related policies with respect to risk assessment and risk management. 
          - ii. Review with management the Company’s cybersecurity and other information technology risks, controls and procedures, including the Company’s plans to mitigate cybersecurity risks and respond to data breaches. 
          - iii. Review with management the Company’s risk exposures in other areas, as the Committee deems necessary or appropriate from time to time. 
          - iv. Review with management the Company’s (a) programs for promoting and monitoring compliance with applicable legal and regulatory requirements, and (b) major legal and regulatory compliance risk exposures and the steps management has taken to monitor or mitigate such exposures. 
          - v. Review the status of any significant legal and regulatory matters and any material reports or inquiries received from regulators or government agencies that reasonably could be expected to have a significant impact on the Company’s financial statements. 
    - *Insurance.* Review and establish any appropriate changes to the insurance coverages for the Company’s directors and officers. 
    - **Internal Audit.** 
          - i. The Committee also will review the adequacy of the Company’s internal audit charter annually, and approve any changes the Committee determines appropriate, if applicable. 
          - ii. Approve the annual internal audit plan and all major changes to the plan. Review the internal audit activity’s performance relative to its plan. 
    - [Code of Conduct](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/). Review the process for communicating the Company’s Code of Conduct to Company personnel, and for monitoring compliance therewith. 
    - [Related-Person Transactions](https://about.gitlab.com/handbook/legal/gitlab-related-party-transactions-policy/). The Audit Committee shall review related-person transactions under the Company’s Related Person Transaction Policy and applicable accounting standards on an ongoing basis and such transactions shall be approved or ratified by the Audit Committee. 
1. **Committee Meeting Procedures and Administration:**
    - Meetings. The Committee shall meet in person, telephonically, or via video conference as often as it deems necessary in order to perform its responsibilities. 
    - A quorum of the Committee for the transaction of business will be a majority of its members. 
    - The Committee also may act by unanimous written consent in lieu of a meeting in accordance with the Company’s Bylaws. 
    - The Committee shall periodically meet separately with: (i) the Independent Auditors and (ii) Company management. 
    - The Committee will maintain written minutes of its meetings and copies of its actions by written consent and will file such minutes and copies of written consents with the minutes of the meetings of the Board. 
    - The Committee will regularly report to the Board on its activities. 
1. **Advisors.** The Committee shall have the authority, without further action by the Board, to engage and determine funding for such independent legal, accounting and other advisors or experts as it deems necessary or appropriate to carry out its responsibilities and will have direct oversight of the work performed by such advisors and the right to terminate their services. The Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of such advisors as established by the Committee. 
1. **Investigations.** The Committee shall have the authority to conduct or authorize investigations into any matter within the scope of its responsibilities, as it shall deem appropriate, including the authority to request any officer, team member or advisor of the Company to meet with the Committee or any advisors engaged by the Committee. 
1. **Review of Committee Composition, Performance and Charter.** The Committee will evaluate on an annual basis the Committee’s composition and performance. The Committee also will review and reassess the adequacy of this Charter annually, and recommend to the Board any changes the Committee determines appropriate. 
1. **Delegation of Authority.** The Committee may from time to time, as it deems appropriate and to the extent permitted under applicable law, the Company’s Certificate of Incorporation and Bylaws, form and delegate, either exclusively or non-exclusively, authority to subcommittees. Subcommittees of the Committee will consist of one or more members of the Committee who will regularly report on their activities to the Committee.

### Audit Committee Agenda Planner

We review the below topics no less frequently than the following schedule:

#### Management, Accounting and Reporting

|#|Topics                                                              | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---:  |
| 1 |Accounting policies                                               |       |       |       |       |    X   |
| 2 |Significant estimates and judgements                              |       |       |       |       |    X   |
| 3 |New accounting standards – impact and implementation plan         |       |       |       |       |    X   |
| 4 |Review of financial Statements <br> (if applicable, GAAP and Non-GAAP financials/metrics) |   X   |    X   |   X   |   X |       |
| 5 |Treasury                                                          |       |   X   |   X   |   X   |       |
| 6 |Investment                                                        |       |   X   |       |       |       |
| 7 |Review of financial statement risk                                |       |   X   |       |       |       |
| 8 |Insurance coverage update                                         |       |       |   X   |       |       |
| 9 |Close process                                                     |       |    X  |       |       |       |
|10 |Stock transactions                                                |       |       |       |       |   X   |
|11 |Tax audits / Taxes                                                |       |       |       |       |   X   |
|12 |Guidance model (if applicable)                                    |       |       |       |       |   X   |
|13 |Audit Update                                                      |   X   |   X   |   X   |   X   |       |
|14 |Attrition                                                         |   X   |   X   |   X   |   X   |       |
|15 |Quarterly Calendar                                                |   X   |       |       |       |       |
|16 |Organization Overviews - Accounting, Finance, Tax, FP&A, Investor Relations       |       |   X   |       |       |       |

#### People Division

|#|Topics                                                                  | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Global staffing update, succession plan and continuous improvement   |       |   X   |       |       |      |
| 2 | EEO audits                                                           |       |   X   |       |       |      |
| 3 | Payroll, Compensation and hiring                                     |       |   X   |       |       |      |
| 4 | Organization Overview                                                |       |   X   |       |       |      |

#### Legal, Risk and Compliance

|#| Topics                                                                             | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------ | :---: | :---: | :---: | :---: |:---: |
| 1 | Compliance to business conduct (including hotline complaints and code of conduct violations).|       |       |       |    |  X  |
| 2 | Code of conduct, Related party transactions and other policy reviews              |      |       |       |     |    X  | 
| 3 | Legal risk assessment updates                                                     |      |       |       |     |    X  |    
| 4 | Regulatory compliance                                                             |      |   X   |       |     |       |  
| 5 | Privacy                                                                           |      |   X   |       |     |       |  
| 6 | Committee annual assessment                                                       |      |       |       |  X  |       |
| 7 | Litigation                                                                        |      |       |       |     |   X   |
| 8 | Approval of minutes                                                               |   X  |   X   |   X   |  X  |       |
| 9 | Review of Audit committee charter                                                 |   X  |       |       |     |       | 
| 10 | Related party transactions                                                       |      |       |       |     |   X   | 
| 11 | Organization Overview                                                            |      |   X   |       |     |       |

#### System

|#| Topics                                                                 | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Information Technology System updates                                |   X   |   X   |   X   |   X   |      |
| 2 | Organization Overview                                                |       |   X   |       |       |      |


#### Security Compliance

|#| Topics                                                           | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Cyber risk assessment                                                |       |   X   |       |       |      |      
| 2 | Security update                                                      |       |   X   |   X   |   X   |      |
| 3 | Finance application system reviews                                   |       |   X   |   X   |   X   |      |
| 4 | Organization Overview                                                |       |   X   |       |       |      |


#### Internal Audit

|#| Topics                           | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Internal audit and global annual plan                                                     |       |       |       |   X   |      |
| 2 | Internal audit activity report and annual plan update                                     |   X   |   X   |   X   |   X   |      |
| 3 | SOX - Internal control over financial reporting assessment and deficiencies status update |   X   |   X   |   X   |   X   |      |
| 4 | Internal controls (pre-Sox)                                                               |       |   X   |       |       |      |
| 5 | Internal audit charter review                                                             |   X   |       |       |       |      |
| 6 | Fraud Risk assessment                                                                     |       |   X   |       |       |      |
| 7 | Annual assessment of internal audit                                                       |       |       |   X   |       |      |
| 8 | Organization Overview                                               |       |   X   |       |        |     |

#### External Audit

|#| Topics                                                                        | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :-------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Global audit plan and fees/Appoint External Auditor                     |       |       |       |   X   |      |
| 2 | Year-end audit results and required communications, as applicable       |   X   |   X   |   X   |   X   |      |
| 3 | Annual assessment of audit firm, engagement team and lead audit partner |       |       |   X   |       |      |
| 4 | Independence review                                                     |       |       |       |   X   |      |
| 5 | Audit                                                                   |   X   |   X   |   X   |   X   |      |


#### Closed session 

|#| Topics                                     | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Executive Session with invitees          |       |       |       |       |   X  |
| 2 | Chief Legal Officer                      |   X   |   X   |   X   |   X   |      |
| 3 | Chief Finance Officer                    |   X   |   X   |   X   |   X   |      |
| 4 | Approvals                                |   X   |   X   |   X   |   X   |      |
| 5 | External Auditor                         |   X   |   X   |   X   |   X   |      |

<br>

### Audit Committee Meeting Deck Preparation Guidelines

**Responsibility: [Chief Financial Officer](https://about.gitlab.com/company/team/#brobins) / [Principal Accounting Officer](https://about.gitlab.com/company/team/#daleb04)**

1. All the audit committee decks should be saved in [google drive](https://drive.google.com/drive/folders/1nqK9DZC84qbV6b6rKwjt0NgpVjip1WnW).
1. Refer to GitLab [Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit#gid=519993910) and identify Audit Committee Meeting Date.
1. Copy the [Format](https://docs.google.com/presentation/d/15k15TYvTGkxZizBds1geY3lTlIq1nfU9ofwwynoY9dM/edit#slide=id.g6478e21bce_0_0) of the Audit Committee meeting deck and rename the deck as **Audit Committee Meeting Month,MM, Day**
1. | Update the agenda slide of the deck by                                                                                                                                 |
    | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | a) Referring to the [master calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) -> Audit Committee calendar tab FY22 |
    | b) [Handbook](https://about.gitlab.com/handbook/board-meetings/committees/audit/#audit-committee-agenda-planner) for Audit Committee meeting agenda items                                                      |
    | c) Audit Committee meeting [notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit)                                              |
1. Set up a meeting with Principal Accounting Officer to review and update all the agenda items.
1. Once the agenda is finalized, create slides for each agenda item and assign to respective DRI’s with a due date for completion; at least  10 days before the meeting.
1. Tag all the DRI’s on **#Audit Committee** slack channel, linking the Deck and communicating the due date for completion of the deck.
1. Follow up with all the DRI's at least a week before the due date.
1. Once respective DRI's update their slides, review the format, update slides to ensure format is consistent across all the slides.
1. Set up a call with the Principal Accounting officer on the due date of the deck/ next immediate day to review the deck. Make necessary changes based on the review.
1. Set up a call for CFO's review along with Principal Accounting Officer. Make necessary changes to the deck based on CFO's review.
1. Principal Accounting Officer to send the deck to all Audit Committee members (cc DRI's) at least a week before the meeting.
1. Update Audit Committee [meeting notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit#heading=h.nu90jml2xhx2) with agenda items and DRI’s.
1. On the meeting day
    - Make note of the follow-up items, add them to the agenda under ask from the Audit Committee section in in the [AC calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) along with the due date.
    - Schedule a call after the day of the Audit Committee meeting with the Principal Accounting Officer to review the agenda for the next meeting.
