---
layout: handbook-page-toc
title: "Nominating and Corporate Governance Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Nominating and Corporate Governance Committee Composition

- **Chairperson:** Matthew Jacobson
- **Members:** Sid Sijbrandij, Sue Bostrom
- **Management DRI:** Chief Legal Officer

## Nominating and Corporate Governance Committee Charter

**THIS CHARTER WAS APPROVED BY THE BOARD ON 2021-03-18**

1. **Purpose**: The purpose of the Nominating and Corporate Governance Committee (the “**Committee**”) of the Board of Directors (the “**Board**”) of GitLab, Inc. (the “**Company**”) is to ensure that the Board is properly constituted to meet its fiduciary obligations to stockholders and the Company, and to assist the Board with respect to corporate governance matters, including:
    - identifying, considering and recommending candidates for membership on the Board;
    - developing and recommending corporate governance guidelines and policies for the Company; 
    - advising the Board on corporate governance matters and Board performance matters, including recommendations regarding the structure and composition of the Board and Board committees; and
    - assisting the Board in overseeing any Company program relating to corporate responsibility and sustainability [^1]. 
    This charter (the “**Charter**”) sets forth the authority and responsibilities of the Committee in fulfilling its purpose.
1. **Structure and Membership**: The Committee will consist of three or more members of the Board, with the exact number determined from time to time by the Board. All members of the Committee will be appointed by the Board and will serve at the Board’s discretion. Members of the Committee may be replaced or removed by the Board at any time, with or without cause. Resignation or removal of a director from the Board, for whatever reason, will automatically constitute resignation or removal, as applicable, from the Committee. 
    - Qualifications: Members of the Committee must meet the following criteria as well as any additional criteria required by applicable law, or such other qualifications as are established by the Board from time to time: 
    - Each member of the Committee will:
         - be free from any relationship that, in the opinion of the Board, would interfere with the exercise of independent judgment as a Committee member; and
         - meet any other requirements imposed by applicable law, regulations or rules, subject to any applicable exemptions.  
    - The Board may appoint a member of the Committee to serve as the chairperson of the Committee (the “**Chair**”). If the Board does not appoint a Chair, the Committee members may designate a Chair by their majority vote.  The Chair will work with management to set the agenda for Committee meetings and conduct the proceedings of those meetings.
1. **Authority and Responsibilities**: The principal responsibilities and duties of the Committee in serving the purposes outlined in Section I of this Charter are set forth below.
These duties are set forth as a guide, with the understanding that the Committee will carry them out in a manner that is appropriate given the Company’s needs and circumstances.
The Committee may supplement them as appropriate and may establish policies and procedures from time to time as are consistent with its purpose and applicable law, rules and regulations, as the Board may request or prescribe, or as the Committee deems necessary or appropriate consistent with its purpose. 

The responsibilities and authority of the Committee will include:
    - _Board Composition, Independence and Nominating Duties_:
        1. Periodically consider and assess the desirability of developing policies regarding director nomination processes and, if the Committee determines it appropriate to have such policies, to develop and recommend such policies. 
        1. Periodically consider and make recommendations to the Board regarding the size, structure and composition of the Board and its committees. 
        1. Develop and recommend to the Board the desired qualifications, expertise and characteristics of Board members. 
        1. Identify, consider and assist in recruiting qualified candidates for Board membership, consistent with criteria approved by the Board. 5. Oversee inquiries into the backgrounds and qualifications of potential candidates for membership on the Board. 
        1. Review, and recommend to the Board for its determination, the independence of the non-employee directors and members of the Committee, the Audit Committee, Compensation Committee or other independent committees of the Board. 
        1. Consider nominations properly submitted by the Company’s stockholders in accordance with the procedures set forth in the Company’s Certificate of Incorporation or Bylaws or determined by the Committee from time to time. 
        1. Recommend for the Board’s selection all nominees to become members of the Board by appointment or to be proposed by the Board for election by the stockholders. 
    - _Corporate Governance Duties_:
        1. Consider, develop and make recommendations to the Board regarding Company policies, including corporate governance guidelines, and revisions to such policies. 
        1. Recommend that the Board establish special committees as may be desirable or necessary from time to time in order to address interested director, ethical, legal or other matters that may arise. 
        1. Consider the Board’s leadership structure, including the separation of the Chairman and Chief Executive Officer roles and/or appointment of a lead independent director of the Board, either permanently or for specific purposes, and make such recommendations to the Board with respect thereto as the Committee deems appropriate. 
        1. Periodically consider procedures for stockholder communications with members of the Board. 
        1. Oversee a process for evaluating on an annual basis the performance of the Board, each committee and each director. 
        1. Make such recommendations to the Board and its committees as the Committee may consider necessary or appropriate and consistent with its purpose, and take such other actions and perform such other services as may be referred to it from time to time by the Board. 
        1. Discharge the responsibilities as set forth in such policies, codes and guidelines approved by the Board. 
    - _Corporate Responsibility_: The Committee will 
        1. Develop, recommend to the Board and oversee any of the Company’s programs relating to corporate responsibility and sustainability, including environmental, social and corporate governance matters. Review and assess with management the Company’s performance, risks, controls and procedures relating to corporate responsibility and sustainability.
        1. Oversee the Company’s engagement efforts with stockholders and other key stakeholders, including non-governmental organizations and key environmental, social and governance ratings agencies. 
1. **Studies and Advisors:**
    The Committee, in discharging its responsibilities, may conduct, direct, supervise or authorize studies of, or investigations into, matters within the Committee’s scope of responsibility, with full and unrestricted access to all books, records, documents, facilities and personnel of the Company.  The Committee has the sole authority and right, at the expense of the Company, to retain legal counsel and other consultants, accountants, experts and advisors of its choice to assist the Committee in connection with its functions, including any studies or investigations.  The Committee will have the sole authority to approve the fees and other retention terms of such advisors.  The Company will provide for appropriate funding, as determined by the Committee, for:
    -	payment of compensation to any search firm, legal counsel and other consultants, accountants, experts and advisors retained by the Committee; and
    -	ordinary administrative expenses of the Committee that are necessary and appropriate in carrying out its functions.
    Irrespective of the retention of legal and other consultants, accountants, experts and other advisors to assist the Committee, the Committee shall exercise its own judgment in fulfillment of its functions.
1. **Meetings, Actions Without A Meeting And Staff:** The Committee will meet with such frequency as is determined appropriate by the Committee.  The Chair, in consultation with the other member(s) of the Committee, will set the dates, times and places of such meetings.  The Chair or any other member of the Committee may call meetings of the Committee by notice in accordance with the Company’s Bylaws.  A quorum of the Committee for the transaction of business will be a majority of its members.  Meetings may be held via tele- or video-conference.  The Committee may also act by unanimous written consent in lieu of a meeting in accordance with the Company’s Bylaws.  Subject to the requirements of this Charter, and applicable law, rules and regulations, the Committee and the Chair may invite any director, executive or employee of the Company, or such other person, as it deems appropriate in order to carry out its responsibilities, to attend and participate (in a non-voting capacity) in all or a portion of any Committee meeting.  The Committee may exclude from all or a portion of its meetings any person it deems appropriate in order to carry out its responsibilities.  The Chair will designate a secretary for each meeting, who need not be a member of the Committee.  The Company will provide the Committee such staff support as it may require.
1. **Minutes and Reports:**
    The Committee will maintain written minutes of its meetings and copies of its actions by written consent, and will make such minutes and copies of written consents available to the other members of the Board and cause them to be filed with the minutes of the meetings of the Board.  The Chair will report to the Board from time to time with respect to the activities of the Committee, including on significant matters related to the Committee’s responsibilities and the Committee’s deliberations and actions.
1. **Delegation of Authority:** The Committee may from time to time, as it deems appropriate and to the extent permitted under applicable law, and the Company’s Certificate of Incorporation and Bylaws, form and delegate authority to subcommittees. Subcommittees of the Committee will consist of one or more members of the Committee who will regularly report on their activities to the Committee. 
1. Review of Committee Composition, Performance and Charter: The Committee will evaluate on an annual basis the Committee’s composition and performance. The Committee also will review and reassess the adequacy of this Charter annually, and recommend to the Board any changes the Committee determines appropriate.

## Footnotes 

[^1]: Many technology companies are starting to include corporate responsibility in their charters as a result of increased focus on this by a variety of stakeholders, including employees, proxy advisory firms and institutional stockholders.
