---
layout: markdown_page
title: "Gerrit"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Gerrit is a free, web-based team code collaboration tool. Software developers in a team can review each other's modifications on their source code using a Web browser and approve or reject those changes. It integrates closely with Git. It was originally developed as a code review system for Android development. Gerrit includes code diffs, discussions, a and a git server.

In contrast, GitLab provides a git server, with issues and issue tracking, as well as code review, and features to automate the entire DevOps lifecycle.

## Comments/Anecdotes
* From a customer who used Gerrit:
  * Code Review lacked collaboration and communication: commit had 1 single thread, review by commit instead of branch.
  * Wanted to set up forks but Gerrit has no concept of forks
  * Unable to delete projects, creates a cluttered namespace
  * No group ability
  * Not intuitive - additional training required for any more complex workflows

* From GitLab Solutions Architect who used to work with Gerrit:
   > Gerrit takes a different approach to branching than standard Git.  In Gerrit, essentially every branch is a protected branch, but there is a much lower need for branches because Gerrit will create them automatically as they’re needed.  As a developer, I can work on the master branch, and when I push to origin, Gerrit automatically creates a “virtual” branch called refs/for/master in the origin and puts my change on that branch.  This is similar to the GitLab merge request creating a branch when the MR is created in an issue.  The difference is that there are refs/for/master is virtual and so there are many - I have a refs/for/master, you have a refs/for/master, each person who pushes has one in the origin repo.  The changes are held on this virtual branch until the review is completed per the rules, at which time it’s merged and the virtual branch is deleted.  Gerrit really encourages merging to master and maintaining only one branch, which is a best practice, I think.  The default Gerrit workflow is actually very similar to GitLab Flow but is different from GitHub or FOSS Git.
   >
   > Gerrit has a Jenkins plugin that knows a lot about Gerrit.  When I push to origin, Gerrit can kick off a Jenkins build by triggering Jenkins rather than by Jenkins polling Gerrit.  Gerrit passes the refs/for/master branch info so Jenkins builds just the change that was pushed.  The Review request can be set up to require a successful build (verification) and the reviewers won’t even be notified they have a review to do until the build is successful.
   >
   > Rules can be set up for things like “successful build plus one reviewer” or “successful build plus two reviewers”.  So the build gives a +1 on verification, and then each reviewer can give +1 on the review.
   >
   > Reviews are different than merge requests because you may assign reviews to people that wouldn’t normally have merge permissions on a branch.  For example, a senior developer or architect may make a change that she wants a junior engineer to learn from.  She can add that person to the review and give them a way to manage reviewing the change.  This may be similar to the Approver field on a merge request.
   >
   > Gerrit supports read and write privileges on branches, not just write privileges.  This makes it more appealing to regulated industries like defense and orgs that use a lot of contractors and may not want an entire repo going on the laptop of a contractor.  It’s easier to assign permissions than it is to break up a repo into parts the contractor can see and parts they can’t.  This is why CollabNet chose Gerrit as it’s Git of choice because it enabled them to do branch level permissions in Git like they do in Subversion.
   >
   > Negatives:
   >   * Gerrit has lots of options.  Lots.  The options page is reminiscent of the cockpit of a 747 there are so many switches. This makes it tough to adopt for teams without dedicated Gerrit experts and/or expert developers.
   >   * Gerrit doesn’t use the standard hook scripts, so migrating to/from Gerrit where process is enforced by pre-commit hooks takes extra work, potentially lots of extra work.
   >
   > There are a lot of similarities between GitLab with Merge Requests and CI Pipelines and Gerrit with Jenkins, similarities that other Gits don’t have, nor does Jenkins by itself.


## Resources
* [Gerrit website](https://www.gerritcodereview.com/index.html)
* [Gerrit wikipedia](https://en.wikipedia.org/wiki/Gerrit_%28software%29)

## Comparison
