---
layout: markdown_page
title: Scheduling Live Upgrade Assistance
description: "Live Upgrade Assistance FAQ"
---


The GitLab support team is here to help. As a part of our [Priority Support](index.html#priority-support) we offer Live Upgrade Assistance. That is, we'll host a live screen share session to help you through the process and ensure there aren't any surprises.

## On This Page
{:.no_toc}

- TOC
{:toc}

## How much advanced notice must I give?

For support to properly assist you, the earlier you can notify us, the better. Our minimum requirements for notification are:

* One week advanced notice for upgrades during [GitLab Support hours](index.html#definitions-of-gitlab-support-hours)
* Two weeks advanced notice for upgrades not during [GitLab Support hours](index.html#definitions-of-gitlab-support-hours)

If you cannot meet our minimum advanced notice period for your planned upgrade,
we may not be able to go ahead with a Live Upgrade Assistance call. In this
case: 

* Should time permit we will get a support engineer to review your upgrade and rollback plans.
* You still have [access to emergency support](https://about.gitlab.com/support/#how-to-trigger-emergency-support) should you encounter a production outage as a result of your upgrade.


## What information do I need to schedule Live Upgrade Assistance?

First, confirm that nothing about your instance would make the request [out of scope for support](https://about.gitlab.com/support/statement-of-support.html#out-of-scope).

Then, please provide all the relevant information you can so that we will be best positioned to assist you. At a minimum, we require:

1. An upgrade plan
1. A rollback plan
1. Updated architecture documentation
1. The point of contact for support to use (email address preferred)
1. The time of the upgrade (please include date, time, and timezone)
1. Any additional relevant information (e.g. *We've had no issues simulating this upgrade in our staging environment*)

## Can we use custom scripts for upgrade/rollback plans?

You can do so, however we cannot review the scripts themselves to determine if they are viable. Generally speaking, our upgrade documentation is the single source of truth for how to best carry out an upgrade.

If issues do occur during the upgrade window and you are running a custom script, it is likely the advice from support will be to utilize your rollback plan. It is important to know that should issues arise while using custom scripts, Support will recommend following the exact steps from our documentation on future attempts.

## How do I schedule Live Upgrade Assistance?

### Global Support
If you have a [Technical Account Manager](/handbook/customer-success/tam/#what-is-a-technical-account-manager-tam), reach out to them with the above required information. Your Technical Account Manager will follow the [Live Upgrade Assistance workflow](https://about.gitlab.com/handbook/support/workflows/live-upgrade-assistance.html).

If you don't have a Technical Account Manager, but still have Priority Support - please [contact support](https://about.gitlab.com/support/#contact-support) with the above required information directly to schedule.

### US Federal Support
If your organization meets the [requirements](https://about.gitlab.com/support/#limitations) for GitLab's US Federal Support you may use the [Live Upgrade Assistance Request](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001434131) form to begin the process. For efficiency, please include the [required information](#what-information-do-i-need-to-schedule-live-upgrade-assistance) when opening the case. Once Support has recieved the case an engineer will review the upgrade plan and make any notes/suggestions as well as provide a single use Calendly link to schedule the date and time of the upgrade.

## What does Live Upgrade Assistance Look like?

After confirming that all relevant information has been provided a GitLab Support Engineer will send an invitation for a call. We aim to have this invitation with you at least one business day before the date of the upgrade.
A GitLab Engineer will join you for the first 30 minutes of your upgrade, to help kick things off and ensure that
you're set up for success by:

1. ensuring that there's an open ticket for the upgrade that they are monitoring
1. going over the upgrade plan once more
1. verifying that there is a roll-back plan in place should things not go according to plan.

At that point, the engineer will drop off the call and watch the ticket for any updates for the duration
of the upgrade. If required, they'll rejoin the ongoing call to troubleshoot any issues.

Once the upgrade is complete, and has passed your post-upgrade success criteria, please be sure to send an
update to the ticket that was opened so the Engineer knows they can go offline.

If there haven't been any updates for some time, the Engineer assisting may rejoin the call or send an update to the
ticket requesting an update.

If the upgrade is taking longer than expected and the shift of the assigned engineer is about to end, they will notify you through the support ticket that they are no longer available. If you need assistance after that point, please page the on-call engineer to help troubleshoot.

## What versions of GitLab will you support upgrading between?

As noted in our Statement of Support, we [support the current major version and two previous major versions](statement-of-support.html#we-support-the-current-major-version-and-the-two-previous-major-versions). If you're upgrading from a version of GitLab that is no longer supported, Live Upgrade Assistance may not be an option. If you're in this situation, please discuss options with your Technical Account Manager or your Account Manager for Professional Services options.

## What if I don't give a week notice? Will I still be supported?

As a part of [Priority Support](index.html#priority-support), you're also entitled to **24x7 uptime Support**. If
you encounter any issues that are leading to downtime during your upgrade, you can page the on-call engineer to help troubleshoot.

Please provide as much context as you can, including an upgrade plan when you open your emergency ticket.

Do note, that in some cases the best option available may be to invoke your roll-back plan and reschedule the upgrade.
